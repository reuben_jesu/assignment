import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }
  setItem(users){
    localStorage.setItem('users',JSON.stringify(users));
  }
  getItem(){
    return JSON.parse(localStorage.getItem('users'));
  }

}
