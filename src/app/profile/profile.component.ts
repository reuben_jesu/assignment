import { Component, OnInit, Input, ViewChild } from '@angular/core';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Input() currUser:any;
  @ViewChild('gmap') gmapElement: any;
  lat = 51.678418;
  lng = 7.809007;
  constructor() {
    console.log('currUser',this.currUser);

   }

  ngOnInit(): void {
    console.log('currUser',this.currUser);
    
  }

}
