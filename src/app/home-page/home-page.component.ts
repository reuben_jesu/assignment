import { Component, OnInit } from '@angular/core';
import {HttpService} from '../service/http-service/http.service';
import {LocalStorageService} from '../service/local-storage-service/local-storage.service';

import { Router } from '@angular/router';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  userList: any;
  constructor(private httpService: HttpService,private router: Router,private localStorService: LocalStorageService) { }

  ngOnInit(): void {
    this.httpService.getData().subscribe((res)=>{
      console.log(res);
      this.userList = res['users'];
      this.localStorService.setItem(this.userList);
      console.log('  this.userList',  this.userList);
      
    })
  }
  seeProfileDetails(i){
    this.router.navigateByUrl('profile/'+i)
  }
}
