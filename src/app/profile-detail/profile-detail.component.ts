import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http-service/http.service';
import { LocalStorageService } from '../service/local-storage-service/local-storage.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: ['./profile-detail.component.css']
})
export class ProfileDetailComponent implements OnInit {
  isProfile : boolean = true;
  isPost : boolean = false;
  isGallery : boolean = false;
  isTodo : boolean = false;
  currUser: any = {};
  constructor(private http: HttpService,private locStorService: LocalStorageService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    console.log('router url',);
    let id=this.route.snapshot.paramMap.get('id');
    let users = this.locStorService.getItem();
    console.log('user',users);
    
    let user = users.filter(x =>{
      console.log(x,x.id,parseInt(id));
      
      return parseInt(x.id) == parseInt(id);
    }); 
    this.currUser = user[0];
    console.log('currUser',this.currUser);
    
  }
  goToHome(){
    this.router.navigateByUrl('home-page');
  }

}
